# Data Importer for Family Tree Maker

To use this importer, install python3

No dependencies needed, this importer uses the built-in JSON module

### Steps

First, clone this repo

```
cd scripts/importer
```

```
python3 parser.py > output.json
```

Copy, paste the contents of `output.json` to the `src/data/datafile.json` file.

Then, run `npm start` to see the output
