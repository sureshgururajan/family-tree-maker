import { getName } from "./familyTreeUtils";

export class Node {
  constructor(id, name) {
    this.id = id;
    this.name = name;
    this.children = {};
  }

  getName() {
    return this.name;
  }

  getId() {
    return this.id;
  }

  getChildren() {
    var children = [];
    for (var child of Object.keys(this.children)) {
      children.push(this.children[child]);
    }
    return children;
  }

  addChild(node) {
    this.children[node.getId()] = node;
  }
}

export class Tree {
  constructor(data, node, children) {
    this.root = node;
    this.nodes = {};
    this.nodes[this.root.getId()] = this.root;
    this.data = data;
    this.addChildren(this.root, children);
  }

  addChildrenToNodeId(nodeId, childrenIds) {
    if (nodeId in this.nodes) {
      this.addChildren(this.nodes[nodeId], childrenIds);
    }
  }

  addChildren(root, childrenIds) {
    childrenIds.forEach((child) => {
      const name = getName(this.data, child);
      this.nodes[child] = new Node(child, name);
      root.addChild(this.nodes[child]);
    });
  }

  dfs_helper(node, visited, callback) {
    visited[node.getId()] = true;
    if (node.getChildren().length === 0) {
      return callback(node, [], []);
    } else {
      var path = [];
      for (let child of node.getChildren()) {
        if (!(child.getId() in visited)) {
          visited[child.getId()] = true;
          var result = this.dfs_helper(child, visited, callback);
          path.push(result);
        }
      }
      return callback(node, node.getChildren(), path);
    }
  }

  dfs(f1, f2) {
    let visited = {};
    return this.dfs_helper(this.root, visited, f1, f2);
  }
}
