import { Autocomplete, TextField } from "@mui/material";

export function Members(props) {
  return (
    <Autocomplete
      disablePortal
      id="familyMembers"
      options={props.familyMembers}
      sx={{ maxWidth: 300 }}
      renderInput={(params) => (
        <TextField {...params} label="Select a member" />
      )}
      defaultValue={props.familyMembers[0]}
      onChange={(event, selection) => {
        props.onChange(selection);
      }}
    />
  );
}
