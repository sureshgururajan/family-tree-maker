import { Button, Grid, Snackbar } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import { AuthContext } from "../utils/context";
import {
  createRelationship,
  getData,
  getMembers,
} from "../utils/familyTreeUtils";
import { Members } from "./Members";
import { RelationshipTypeSelector } from "./RelationshipTypeSelector";

class NewRelationship extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      familyMembers: [],
      loaded: false,
      selectedSource: {},
      selectedType: {},
      selectedTarget: {},
      createNewSnackbar: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.handleCloseSnackbar = this.handleCloseSnackbar.bind(this);
  }

  async componentDidMount() {
    let data = await getData(this.context);
    let members = getMembers(data);
    this.setState({
      familyMembers: members,
      loaded: true,
    });
  }

  handleCloseSnackbar() {
    this.setState({
      createNewSnackbar: false,
    });
  }

  handleChange(source, id) {
    if (source === "source") {
      this.setState({
        selectedSource: id,
      });
    } else if (source === "type") {
      this.setState({
        selectedType: id,
      });
    } else {
      this.setState({
        selectedTarget: id,
      });
    }
  }

  async handleCreate() {
    let relationship = [
      this.state.selectedSource,
      this.state.selectedType,
      this.state.selectedTarget,
    ];
    await createRelationship(relationship, this.context);
    let data = await getData(this.context);
    this.setState({
      allMemberInfo: data,
      createNewSnackbar: true,
    });
  }

  render() {
    if (!this.state.loaded) {
      return <div>Loading ...</div>;
    }
    return (
      <Box sx={{ width: "100%" }}>
        <Grid container spacing={2}>
          <Snackbar
            open={this.state.createNewSnackbar}
            autoHideDuration={2000}
            message="Successfully created"
            onClose={this.handleCloseSnackbar}
          />
          <Grid item xs={12} md={4}>
            <Members
              familyMembers={this.state.familyMembers}
              onChange={(id) => this.handleChange("source", id)}
            />
          </Grid>
          <Grid item xs={12} md={4}>
            <RelationshipTypeSelector
              onChange={(id) => this.handleChange("type", id)}
            />
          </Grid>
          <Grid item xs={12} md={4}>
            <Members
              familyMembers={this.state.familyMembers}
              onChange={(id) => this.handleChange("target", id)}
            />
          </Grid>
          <Grid item xs={12}>
            <Button variant="outlined" onClick={this.handleCreate}>
              Create
            </Button>
          </Grid>
        </Grid>
      </Box>
    );
  }
}

NewRelationship.contextType = AuthContext;

export default NewRelationship;
