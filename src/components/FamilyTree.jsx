import React from "react";
import TreeView from "@mui/lab/TreeView";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import TreeItem, { treeItemClasses } from "@mui/lab/TreeItem";
import { getChildrenIds } from "../utils/familyTreeUtils";
import { alpha, styled } from "@mui/material/styles";

const StyledTreeItem = styled((props) => <TreeItem {...props} />)(
  ({ theme }) => ({
    [`& .${treeItemClasses.iconContainer}`]: {
      "& .close": {
        opacity: 0.3,
      },
    },
    [`& .${treeItemClasses.group}`]: {
      marginLeft: 15,
      paddingLeft: 18,
      borderLeft: `1px dashed ${alpha(theme.palette.text.primary, 0.4)}`,
    },
  })
);

function TreeItemWithChildren(props) {
  return (
    <StyledTreeItem
      nodeId={props.parent.getId()}
      label={props.parent.getName()}
    >
      {props.path}
    </StyledTreeItem>
  );
}

function renderTreeItem(parent, children, path) {
  if (children.length === 0) {
    return <StyledTreeItem nodeId={parent.getId()} label={parent.getName()} />;
  } else {
    return <TreeItemWithChildren parent={parent} path={path} />;
  }
}

class FamilyTreeView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: [this.props.rootId],
      allExpanded: [this.props.rootId],
      selected: [this.props.rootId],
      root: this.props.rootId,
    };
  }

  handleOnNodeSelect(selectedNodeId) {
    const expanded = this.state.expanded;
    const allExpanded = this.state.allExpanded;
    const tree = this.props.tree;
    const data = this.props.data;

    if (this.state.root !== this.props.rootId) {
      this.setState({
        expanded: [this.props.rootId],
        allExpanded: [this.props.rootId],
        selected: [this.props.rootId],
        root: this.props.rootId,
      });
      return;
    }

    this.setState({
      selected: [selectedNodeId],
    });

    this.props.onSelect(selectedNodeId);

    // Expanded before, possibly children loaded
    if (expanded.indexOf(selectedNodeId) > -1) {
      const newExpanded = [...expanded];
      newExpanded.splice(expanded.indexOf(selectedNodeId), 1);
      this.setState({
        expanded: newExpanded,
      });
      return;
    }

    // Previously expanded, then collapsed. Child possibly loaded
    if (allExpanded.indexOf(selectedNodeId) > -1) {
      const newExpanded = [...expanded, selectedNodeId];
      this.setState({
        expanded: newExpanded,
      });
      return;
    }

    // Expand the current node
    const children = getChildrenIds(data, selectedNodeId);

    // If no children, this is a no-op
    if (children.length === 0) {
      this.setState({
        expanded: [...expanded, selectedNodeId],
      });
      return;
    }

    // If has children, add loaded children to tree under the currently selected node
    tree.addChildrenToNodeId(selectedNodeId, children);

    // Update state
    this.setState({
      expanded: [...expanded, selectedNodeId],
      allExpanded: [...allExpanded, selectedNodeId],
    });
  }

  render() {
    return (
      <TreeView
        aria-label="Family Tree"
        defaultCollapseIcon={<ExpandMoreIcon />}
        defaultExpandIcon={<ChevronRightIcon />}
        sx={{ flexGrow: 1 }}
        defaultExpanded={[this.props.rootId]}
        expanded={this.state.expanded}
        defaultSelected={this.state.selected}
        selected={this.state.selected}
        onNodeSelect={(e, selectedNodeId) => {
          this.handleOnNodeSelect(selectedNodeId);
        }}
      >
        {this.props.tree.dfs(renderTreeItem)}
      </TreeView>
    );
  }
}

export default FamilyTreeView;
