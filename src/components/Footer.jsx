import { AppBar, Toolbar, Typography } from "@mui/material";
import * as React from "react";

function Footer() {
  return (
    <AppBar color="primary" sx={{ top: "auto", bottom: 0 }}>
      <Toolbar>
        <Typography component="div">Family Tree Maker</Typography>
      </Toolbar>
    </AppBar>
  );
}

export default Footer;
