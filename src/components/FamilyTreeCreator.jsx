import { Grid, Typography } from "@mui/material";
import * as React from "react";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import NewMember from "./NewMember";
import NewRelationship from "./NewRelationship";
import UpdateProperties from "./UpdateProperties";
import { getAuth, onAuthStateChanged } from "@firebase/auth";
import { AuthContext } from "../utils/context";

function TabPanel(props) {
  const { children, value, index } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

export default class FamilyTreeCreator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
    };
    this.auth = getAuth(this.context);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event, newValue) {
    this.setState({
      value: newValue,
    });
  }

  componentDidMount() {
    onAuthStateChanged(this.auth, (user) => {
      if (user) {
        // stay
      } else {
        this.props.logoutHandler();
      }
    });
  }

  render() {
    return (
      <Grid container spacing={2} sx={{ my: 10 }}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography variant="h5" component="h5" sx={{ mx: 2 }}>
              Create Family Tree
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Box sx={{ width: "100%" }}>
              <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                <Tabs
                  value={this.state.value}
                  onChange={this.handleChange}
                  aria-label="basic tabs example"
                >
                  <Tab label="Add member" />
                  <Tab label="Add relationship" />
                  <Tab label="Update properties" />
                </Tabs>
              </Box>
              <TabPanel value={this.state.value} index={0}>
                <NewMember />
              </TabPanel>
              <TabPanel value={this.state.value} index={1}>
                <NewRelationship />
              </TabPanel>
              <TabPanel value={this.state.value} index={2}>
                <UpdateProperties />
              </TabPanel>
            </Box>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

FamilyTreeCreator.contextType = AuthContext;
