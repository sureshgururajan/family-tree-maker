import MenuIcon from "@mui/icons-material/Menu";
import {
  AppBar,
  Divider,
  Drawer,
  Link,
  List,
  ListItem,
  ListItemText,
  Toolbar,
  Typography,
} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import * as React from "react";

function Header() {
  const drawerWidth = 200;
  const [drawerState, setDrawerState] = React.useState(false);

  function handleMenuClick() {
    setDrawerState(!drawerState);
  }

  return (
    <React.Fragment>
      <div>
        {["left", "right", "top", "bottom"].map((anchor) => (
          <React.Fragment key={anchor}>
            <Drawer
              sx={{
                width: drawerWidth,
                flexShrink: 0,
                "& .MuiDrawer-paper": {
                  width: drawerWidth,
                  boxSizing: "border-box",
                },
              }}
              open={drawerState}
              onClose={() => handleMenuClick()}
              anchor="left"
            >
              <Toolbar>
                <IconButton
                  color="primary"
                  aria-label="open drawer"
                  edge="start"
                >
                  <MenuIcon />
                </IconButton>
                <Typography variant="h5" component="h5" sx={{ mx: 2 }}>
                  Menu
                </Typography>
              </Toolbar>
              <Divider />
              <List>
                {["View", "Create", "About", "Contact", "Logout"].map(
                  (text, index) => (
                    <ListItem
                      button
                      key={text}
                      component="a"
                      href={text.toLowerCase()}
                    >
                      <ListItemText primary={text} />
                    </ListItem>
                  )
                )}
              </List>
              <Divider />
              <List>
                {["Import", "Export", "Share"].map((text, index) => (
                  <ListItem button key={text}>
                    <ListItemText primary={text} />
                  </ListItem>
                ))}
              </List>
              <Divider />
              <List>
                <ListItem button key="Gitlab">
                  <ListItemText primary="Gitlab" />
                </ListItem>
              </List>
              <Divider />
              <Typography variant="p" sx={{ mx: 2, my: 2, px: 2, py: 2 }}>
                Thank you for using this software! ❤️
              </Typography>
              <Divider />
            </Drawer>
          </React.Fragment>
        ))}
      </div>

      <AppBar position="fixed">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
            onClick={() => handleMenuClick()}
          >
            <MenuIcon />
          </IconButton>
          <Link href="/" color="inherit" underline="none">
            <Typography variant="h6" component="div">
              Family Tree Maker
            </Typography>
          </Link>
        </Toolbar>
      </AppBar>
    </React.Fragment>
  );
}

export default Header;
