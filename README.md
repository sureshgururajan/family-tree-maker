# Family Tree Maker - Web App

Open source family tree maker built with React. Currently under development. Will add design docs, etc. later

# How to run?

See https://gitlab.com/familytreemaker/family-tree-maker-deployment

# Legacy Notes below

## How to run

First, checkout the backend and run it:

```bash
git clone git@gitlab.com:sureshgururajan/family-tree-maker-backend.git
cd family-tree-maker-backend
npm install
npm start
```

```bash
git clone https://gitlab.com/sureshgururajan/family-tree-maker.git
cd family-tree-maker
npm install
npm start
```

## If using Docker

Referred https://mherman.org/blog/dockerizing-a-react-app/

### Development

```bash
docker build -t familytreemaker-frontend:alpha .

docker run \
    -it \
    --rm \
    -v ${PWD}:/app \
    -v /app/node_modules \
    -p 3001:3000 \
    -e CHOKIDAR_USEPOLLING=true \
    familytreemaker-frontend:alpha
```

Use `-itd` to run the container detached

### Production

```bash
docker build -f Dockerfile.prod -t familytreemaker-frontend:prod .

docker run -it --rm -p 3001:80 sample:prod

docker push familytreemaker-webapp:latest
```

## Screenshot

### Release v0.0.2 - Current

<img src="./docs/images/screenshot-v0.0.2.png" height="600" />

### Older release images

See https://gitlab.com/sureshgururajan/family-tree-maker/-/tree/main/docs/images
